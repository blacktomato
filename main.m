#import <objc/Object.h>
#import <stdio.h>
#import "Stack.h"

void main() {
  id pilha;

  pilha = [Stack new];
  [pilha push: 12];
  [pilha push: 15];
  [pilha push: 22];
  [pilha push: 79];
  [pilha print];
  [pilha free];
}
