#import <stdlib.h>
#import "Stack-generic.h"

@implementation Stack

#define NULL_LINK (StackLink *) 0

+ new
{
  self = [super new];
  top = NULL_LINK;
  return self;
}

- free
{
  StackLink *next;

  while (top != NULL_LINK)
    {
      next = top->next;
      free ((char *) top);
      top = next;
    }
  return [super free];
}

- push: (int) value
{
  StackLink *newLink;
  
  newLink = (StackLink *) malloc (sizeof (StackLink));
  if (newLink == 0)
    {
      fprintf(stderr, "Out of memory\n");
      return nil;
    }
  newLink->data = value;
  newLink->next = top;
  top = newLink;
  size++;
  
  return self;
}

- (int) pop
{
  int value;
  StackLink *topLink;

  if (0 != size)
    {
      topLink = top;
      top = top->next;
      value = topLink->data;
      free (topLink);
      size--;
    }
  else
    {
      value = 0;
    }
  return value;
}

- (unsigned int) size
{
  return size;
}

- print
{
  StackLink *startLink;
  int i = size;

  startLink = top;
  
  while (startLink) {
    printf("Stack[%d] = %d\n", i, startLink->data);
    startLink = startLink->next;
    i--;
  }
  return self;
}

@end
