#import <Foundation/Foundation.h>
#import <stdio.h>
#import "Stack-generic.h"

void main() {
  id pilha;

  pilha = [Stack new];
  [pilha push: 11];
  [pilha push: 15];
  [pilha push: 22];
  [pilha push: 79];
  [pilha print];
  [pilha pop];
  [pilha pop];
  [pilha print];
  [pilha free];
}
