#import <Foundation/Foundation.h>

typedef struct StackLink {
  struct StackLink *next;
  int data;
} StackLink;

@interface Stack : NSObject
{
  StackLink *top;
  unsigned int size;
}

- free;
- push: (int) anData;
- (int) pop;
- (unsigned int) size;
- (id) print;
@end
