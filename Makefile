.SUFFIXES: .o .m

.m.o:
	$(CC) -c $(CFLAGS) $< -o $@

# Macros

CC=gcc
LDFLAGS=-lobjc
SRCS=main.m Stack.m
OBJS=$(SRCS:.m=.o)
EXECUTABLE=main

.PHONY: all

all: $(EXECUTABLE)

$(EXECUTABLE): $(OBJS)
	$(CC) $(LDFLAGS) -o $@ $(OBJS)

clean:
	@rm -f *.o $(EXECUTABLE) *~

