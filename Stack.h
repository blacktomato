#import <objc/Object.h>

typedef struct StackLink {
  struct StackLink        *next;
  int                     data;
} StackLink;


@interface Stack : Object
{
  StackLink *top;
  unsigned int size;
}

- free;
- push: (int) anInt;
- (int) pop;
- (unsigned int) size;
- (id) print;
@end
